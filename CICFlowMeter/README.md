# CICFlowMeter

## Pull Container Steps

### Log into Docker

    docker login registry.gitlab.com

### Pull Container
##### x86_64 Container
     docker pull registry.gitlab.com/ajack/dockerfiles/cicflowmeter:latest

##### ARM Container
     docker pull registry.gitlab.com/ajack/dockerfiles/cicflowmeter:armv

## Deploy the Container  
##### Can be deployed direclty from repository 

### With interactive Shell:
#### For ARM just subtitute "latest" with "armv"
     docker run -v /full/path/to/input:/input -v /full/path/to/output:/output -it registry.gitlab.com/ajack/dockerfiles/cicflowmeter:latest /bin/bash   

##### __/full/path/to/input(/output) need to be folders created on the host, this will map to the input output folders built in the container.__  

### Without interactive Shell:
     docker run -v /full/path/to/input:/input -v /full/path/to/output:/output -dt registry.gitlab.com/ajack/dockerfiles/cicflowmeter:latest /bin/bash  

# Start application inside of shell
       
##### PCAP files you want the application to read should be stored in the input folder created on the host
##### The CSVs created should get dumped into the output folder (also accessable from the host)

     cd /CICFlowMeter-4.0/bin
     ./cfm /input/ /output/  

__THIS APPLICATION IS NOT REAL TIME IF YOU WANT TO RUN NEW PCAPs IN THE INPUT FOLDER THE COMMAND ABOVE MUST BE RE INITIALIZED__