# Instructions on running the working container can be found in the CICFlowMeter directory

# Docker Registry

     docker login registry.gitlab.com  

# Pull/Push/Build

     docker build -t registry.gitlab.com/ajack/dockerfiles/filename .  
     docker pull registry.gitlab.com/ajack/dockerfiles/filename  
     docker push registry.gitlab.com/ajack/dockerfiles/filename

# Image names

// 3 Options //  

     registry.gitlab.com/ajack/dockerfiles:tag  
     registry.gitlab.com/ajack/dockerfiles/optional-image-name:tag  
     registry.gitlab.com/ajack/dockerfiles/optional-name/optional-image-name:tag  
